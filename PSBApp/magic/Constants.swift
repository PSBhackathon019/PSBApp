//
//  Constants.swift
//  PSBApp
//
//  Created by Ivan Trofimov on 30.09.2017.
//  Copyright © 2017 Ivan Trofimov. All rights reserved.
//

import Foundation
import UIKit
struct Colors {
    static let orange = UIColor.init(red: 215/255, green: 76/255, blue: 13/255, alpha: 1)
    static let yellow = UIColor.init(red: 238/255, green: 215/255, blue: 103/255, alpha: 1)
    static let green = UIColor.init(red: 111/255, green: 187/255, blue: 60/255, alpha: 1)
    static let red = UIColor.init(red: 168/255, green: 58/255, blue: 38/255, alpha: 1)
    static let blue = UIColor.init(red: 92/255, green: 114/255, blue: 151/255, alpha: 1)
    static let darkBlue = UIColor.init(red: 52/255, green: 68/255, blue: 135/255, alpha: 1)

}

enum DColors {
    case blue
    case red
    case yellow
    case green
    case orange
}

enum DTypes {
    case tutorial
    case payment
    case remind
    case tender
}
