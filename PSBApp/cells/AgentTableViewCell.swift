//
//  AgentTableViewCell.swift
//  PSBApp
//
//  Created by Ivan Trofimov on 30.09.2017.
//  Copyright © 2017 Ivan Trofimov. All rights reserved.
//

import UIKit

class AgentTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typesLabel: UILabel!
    @IBOutlet weak var ratingLavel: UILabel!
    
    @IBOutlet weak var photo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            let newVC = vc?.storyboard?.instantiateViewController(withIdentifier: "agentVC") as! AgentViewController
            newVC.data = data
            vc?.show(newVC, sender: vc)
        }
        
        // Configure the view for the selected state
    }
    
    public var vc: UIViewController?
    
    public var data: (name: String, types: [String], inn: String, totalRating: Int, PSBRating: Int, photo: String, contacts: String, address: String, ratingA: Int, ratingB: Int, ratingC: Int)?{
        didSet
        {
            if let data = data {
                photo.image = UIImage.init(named: data.photo)
                nameLabel.text = data.name
                typesLabel.text = "\(data.types[0]) и другие"
                if data.totalRating > 75 && data.PSBRating > 0 {
                    ratingLavel.text = "Надежный контрагент"
                    ratingLavel.textColor = Colors.green
                } else {
                    ratingLavel.text = "Ненадежный контрагент"
                    ratingLavel.textColor = Colors.red
                }
            }
        }
    }
}
