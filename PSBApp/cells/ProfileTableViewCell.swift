//
//  ProfileTableViewCell.swift
//  PSBApp
//
//  Created by Ivan Trofimov on 30.09.2017.
//  Copyright © 2017 Ivan Trofimov. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let abc = CAGradientLayer()
        abc.frame = self.bounds
        abc.colors = [Colors.darkBlue.cgColor, Colors.darkBlue.cgColor, Colors.darkBlue.cgColor, Colors.darkBlue.cgColor, Colors.darkBlue.cgColor, Colors.darkBlue.cgColor, UIColor.white.cgColor]
        let view = UIView()
        view.frame = self.frame
        view.layer.addSublayer(abc)
        self.backgroundView = view
        photo.layer.cornerRadius = 10
        print("ping")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
