//
//  EventTableViewCell.swift
//  PSBApp
//
//  Created by Ivan Trofimov on 30.09.2017.
//  Copyright © 2017 Ivan Trofimov. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lastLineView: UIView!
    @IBOutlet weak var bageView: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bageView.layer.cornerRadius = bageView.frame.height / 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func buttonAction(_ sender: Any) {
        switch type! {
        case .tutorial:
            let newVC = vc?.storyboard?.instantiateViewController(withIdentifier: "tutorialVC") as! TutorialViewController
            newVC.descr = dop1
            newVC.link = dop2
            vc?.navigationController?.show(newVC, sender: vc)
        case.payment:
            let newVC = vc?.storyboard?.instantiateViewController(withIdentifier: "paymentVC") as! PaymentViewController
            newVC.dop2 = dop1
            newVC.dop1 = hrum
            vc?.navigationController?.show(newVC, sender: vc)
        case.tender:
            let newVC = vc?.storyboard?.instantiateViewController(withIdentifier: "tenderVC")
            vc?.navigationController?.show(newVC!, sender: vc)
        default:
            
            break
            
        }
    }
    
    public var vc : UIViewController?
    
    public var isLast = false {
        didSet{
            if isLast {
                lastLineView.backgroundColor = UIColor.clear
                
            } else {
                lastLineView.backgroundColor = UIColor.lightGray
                
            }
        }
    }
    
    public var titleString = "" {
        didSet {
            title.text = titleString
        }
    }
    
    public var subtitleString = "" {
        didSet {
            subtitle.text = titleString
        }
    }
    
    public var color : DColors = .orange {
        didSet {
            switch color {
            case .orange:
                bageView.backgroundColor = Colors.orange
            case .yellow:
                bageView.backgroundColor = Colors.yellow
            case .green:
                bageView.backgroundColor = Colors.green
            case .blue:
                bageView.backgroundColor = Colors.blue
            case .red: 
                bageView.backgroundColor = Colors.red
            }
        }
    }
    
    public var type : DTypes?
    
    public var dop1 = ""
    public var hrum = ""
    public var dop2 = ""
}
