//
//  StudyTableViewCell.swift
//  PSBApp
//
//  Created by Ivan Trofimov on 01.10.2017.
//  Copyright © 2017 Ivan Trofimov. All rights reserved.
//

import UIKit

class StudyTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            let newVC = vc?.storyboard?.instantiateViewController(withIdentifier: "tutorialVC") as! TutorialViewController
            newVC.descr = dataCell!.dop1
            newVC.link = dataCell!.dop2
            vc?.navigationController?.show(newVC, sender: vc)
        }
    }
    
    var title = "" {
        didSet {
            titleLabel.text = title
        }
    }
    var subtitle = "" {
        didSet {
            subtitleLabel.text = subtitle
        }
    }
    
    var vc : UIViewController?
    
    var dataCell : (title: String, subtitle: String, color: DColors, date: String, type: DTypes, code: Int, dop1: String, dop2: String, dop3: [String])?
}
