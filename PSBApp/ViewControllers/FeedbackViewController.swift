//
//  FeedbackViewController.swift
//  PSBApp
//
//  Created by Ivan Trofimov on 01.10.2017.
//  Copyright © 2017 Ivan Trofimov. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = UIColor.white

        // Do any additional setup after loading the view.
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
