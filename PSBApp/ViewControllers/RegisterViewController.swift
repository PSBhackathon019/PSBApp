//
//  RegisterViewController.swift
//  PSBApp
//
//  Created by Ivan Trofimov on 01.10.2017.
//  Copyright © 2017 Ivan Trofimov. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = UIColor.white

        let url : NSURL! = NSURL(string: "https://downloader.disk.yandex.ru/disk/bc48e7ea82e90f33ff24eed2171250c96a152e0f630c15c0a3b90c6e536c01bf/59d0c4e3/Y4egcr9sFZ1onz-bki5HlWPbYseOlKhUOVahCbq-J-ChOm1JHiTi3LrR4E79bQlEAvM4LP3aZJ9lBJS0grDjuw%3D%3D?uid=290652359&filename=AnktetaPSB.pdf&disposition=inline&hash=&limit=0&content_type=application%2Fpdf&fsize=143479&hid=19c5aa8f3d9793492737912956e2106b&media_type=document&tknv=v2&etag=00afb6790a8283fe07e8532e5a145555")
        webView.loadRequest(URLRequest(url: url as URL))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
