//
//  AgentsViewController.swift
//  PSBApp
//
//  Created by Ivan Trofimov on 30.09.2017.
//  Copyright © 2017 Ivan Trofimov. All rights reserved.
//

import UIKit

class AgentsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let data = dataContr.map({$0})
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = UIColor.white
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "agentCell") as! AgentTableViewCell
        cell.vc = self
        cell.data = data[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}
