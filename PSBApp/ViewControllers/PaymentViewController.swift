//
//  PaymentViewController.swift
//  PSBApp
//
//  Created by Ivan Trofimov on 30.09.2017.
//  Copyright © 2017 Ivan Trofimov. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        image.alpha = 0
        titleLabel.text = dop1
        descriptionLabel.text = dop2
        navigationController?.navigationBar.tintColor = UIColor.white

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func payAction(_ sender: Any) {
        if image.alpha == 0 {
            image.alpha = 1
            button.setTitle("Оплачено. Вернуться назад", for: .normal)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    public var dop1 = ""
    public var dop2 = ""
}
