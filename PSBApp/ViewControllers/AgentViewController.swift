//
//  AgentViewController.swift
//  PSBApp
//
//  Created by Ivan Trofimov on 30.09.2017.
//  Copyright © 2017 Ivan Trofimov. All rights reserved.
//

import UIKit

class AgentViewController: UIViewController {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var contactsLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var innLabel: UILabel!
    @IBOutlet weak var totalRatingLabel: UILabel!
    @IBOutlet weak var secondRatingLabel: UILabel!
    @IBOutlet weak var firstRatingLabel: UILabel!
    @IBOutlet weak var thirdRatingLabel: UILabel!
    @IBOutlet weak var additional: UITextView!
    @IBOutlet weak var inviteLabel: UILabel!
    
    @IBOutlet weak var inviteView: UIView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var ratingHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = UIColor.white

        ratingHeight.constant = 0
        ratingView.alpha = 0
        inviteView.alpha = (data!.PSBRating == 0) ? 1 : 0
        if let data = data {
            photo.image = UIImage.init(named: data.photo)
            self.title = data.name
            contactsLabel.text = data.contacts
            addressLabel.text = data.address
            innLabel.text = "ИНН: \(data.inn)"
            firstRatingLabel.text = "Рейтинг в суде: \(data.ratingA)"
            secondRatingLabel.text = "Ругелярность платежей: \(data.ratingB)"
            thirdRatingLabel.text = "Рейтинг заказчиков ПСБ: \(data.ratingC)"
            additional.text = data.types.reduce("", { $0 + " - " + $1 + "\n"})
            inviteLabel.text = "\(data.name) не является клиентом банка Промсвязьбанк. Пригласите его в банк, чтобы совершать моментали платежи и переводы. "
            if data.totalRating > 75 && data.PSBRating > 0 {
                totalRatingLabel.text = "Надежный агент: \(data.totalRating)"
            } else {
                totalRatingLabel.text = "Ненадежный контрагент: \(data.totalRating)"
            }
        }
        // Do any additional setup after loading the view.
    }
    
    func notifyUser(_ title: String, message: String)
    {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "OK",
                                         style: .cancel, handler: nil)
        
        alert.addAction(cancelAction)
        self.present(alert, animated: true)
    }
    
    @IBAction func callAction(_ sender: Any) {
        if let url = URL(string: "tel://\(data!.contacts)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func inviteAction(_ sender: Any) {
        notifyUser("Приглашение отправлено", message: "Вы пригласили \(data?.name ?? "клиента") в наш банк. Скоро вы сможете совершать переводы быстрее и безопаснее")
    }
    
    @IBAction func openAction(_ sender: Any) {
        if ratingHeight.constant == 99 {
            ratingHeight.constant = 0
            ratingView.alpha = 0
            
        } else {
            ratingHeight.constant = 99
            ratingView.alpha = 1
        }
    }
    
    public var data : (name: String, types: [String], inn: String, totalRating: Int, PSBRating: Int, photo: String, contacts: String, address: String, ratingA: Int, ratingB: Int, ratingC: Int)?
    
}
