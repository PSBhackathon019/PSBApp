//
//  ViewController.swift
//  PSBApp
//
//  Created by Ivan Trofimov on 30.09.2017.
//  Copyright © 2017 Ivan Trofimov. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var data = dataEvents.map({$0})
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        navigationController?.navigationBar.barTintColor = Colors.darkBlue
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = UIColor.white
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count + 1
    }
    
    @IBAction func popAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "startVC")
        self.present(vc!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "profileCell")!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell") as! EventTableViewCell
            let cellData = data[indexPath.row - 1]
            if indexPath.row != 0 {
                cell.title.text = cellData.0
                cell.subtitle.text = cellData.1
                cell.color = cellData.color
            }
            cell.hrum = cellData.title
            cell.vc = self
            cell.type = cellData.type
            cell.dop1 = cellData.dop1
            cell.dop2 = cellData.dop2
            cell.isLast = data.count == indexPath.row
            if cellData.type == .remind {
                cell.accessoryType = .none
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 102
        } else {
            return 60
        }
    }
    
    
}


