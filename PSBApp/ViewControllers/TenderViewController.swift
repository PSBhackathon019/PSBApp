//
//  TenderViewController.swift
//  PSBApp
//
//  Created by Ivan Trofimov on 30.09.2017.
//  Copyright © 2017 Ivan Trofimov. All rights reserved.
//

import UIKit

class TenderViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
