//
//  TutorialViewController.swift
//  PSBApp
//
//  Created by Ivan Trofimov on 30.09.2017.
//  Copyright © 2017 Ivan Trofimov. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var descrTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadYoutube(videoID: link)
        descrTextView.text = descr
        navigationController?.navigationBar.tintColor = UIColor.white

        // Do any additional setup after loading the view.
    }

    func loadYoutube(videoID: String) {
        guard
            let youtubeURL = URL(string: "https://www.youtube.com/embed/\(videoID)")
            else { return }
        webView.loadRequest( URLRequest(url: youtubeURL) )
    }
    
    public var link = ""
    public var descr = ""
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
