//
//  SearchViewController.swift
//  PSBApp
//
//  Created by Ivan Trofimov on 30.09.2017.
//  Copyright © 2017 Ivan Trofimov. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var psbSwitch: UISwitch!
    @IBOutlet weak var ratingSwitch: UISwitch!
    
    @IBOutlet weak var table: UITableView!
    var data = dataContr.map({$0})
    var patern = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = UIColor.white

    }
    
    func filter() {
        data = dataContr.filter({ (el) -> Bool in
            let paternResult =
                (el.types.reduce("", {$0 + $1}).lowercased() + el.name.lowercased() + el.contacts + el.inn).range(of: patern.lowercased()) != nil
                    || patern.isEmpty
            return paternResult
        })
    }
    
    @IBAction func switchAction(_ sender: UISwitch) {
       filter()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "agentCell") as! AgentTableViewCell
        cell.vc = self
        cell.data = data[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        patern = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        filter()
        table.reloadData()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
  

}
